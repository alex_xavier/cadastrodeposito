﻿using Cadastros.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cadastros.Models;
using System.Data.Common;
using Dapper;

namespace Cadastros.Repository
{
    public class DepositoRepository : IDepositoRepository
    {
        private DbConnection _connection;

        public DepositoRepository(DbConnection connection)
        {
            _connection = connection;
        }
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Edit(Deposito deposito)
        {
            _connection.Execute(
                @"update mgdbm.dbm_deposito
                     set dep_st_nome = :Nome,
                         dep_dt_cadastro = :DataCadastro,
                         dep_re_area = :Area,
                         dep_ch_status = :Status,
                         org_tab_in_codigo = :EmpTabela,
                         org_pad_in_codigo = :EmpPadrao,
                         org_in_codigo = :EmpCodigo,
                         org_tau_st_codigo = :EmpTipo,
                         est_in_codigo = :EmpId,
                         dep_ch_tipoacesso = :Acesso
                   where dep_in_codigo = :Id"
                , new
                {
                    Id = deposito.Id,
                    Nome = deposito.Nome,
                    DataCadastro = deposito.Data,
                    Area = deposito.Area,
                    Status = deposito.Status,
                    EmpTabela = deposito.Tab,
                    EmpPadrao = deposito.Pad,
                    EmpCodigo = deposito.Org,
                    EmpTipo = deposito.Tau,
                    EmpId = deposito.IdEstruturaUnidade,
                    Acesso = deposito.Acesso
                });
        }

        public IEnumerable<Deposito> Get()
        {
            return _connection.Query<Deposito>(@"SELECT    dep.dep_in_codigo as Id
                                                          ,dep.dep_st_nome as Nome
                                                          ,dep.dep_dt_cadastro as Data
                                                          ,dep.dep_re_area as Area
                                                          ,dep.dep_ch_status as Status
                                                          ,dep.org_tab_in_codigo as Tab
                                                          ,dep.org_pad_in_codigo as Pad
                                                          ,dep.org_in_codigo as Org
                                                          ,dep.org_tau_st_codigo as Tau
                                                          ,dep.est_in_codigo as IdEstruturaUnidade
                                                          ,dep.dep_ch_tipoacesso Acesso
                                                    FROM mgdbm.dbm_deposito dep");
        }

        public Deposito Get(int id)
        {
            return _connection.Query<Deposito>(@"SELECT    dep.dep_in_codigo as Id
                                                          ,dep.dep_st_nome as Nome
                                                          ,dep.dep_dt_cadastro as Data
                                                          ,dep.dep_re_area as Area
                                                          ,dep.dep_ch_status as Status
                                                          ,dep.org_tab_in_codigo as Tab
                                                          ,dep.org_pad_in_codigo as Pad
                                                          ,dep.org_in_codigo as Org
                                                          ,dep.org_tau_st_codigo as Tau
                                                          ,dep.est_in_codigo as IdEstruturaUnidade
                                                          ,dep.dep_ch_tipoacesso Acesso
                                                    FROM mgdbm.dbm_deposito dep
                                                   WHERE dep.dep_in_codigo = :id",
                                               new { id }).SingleOrDefault();
        }

        public void Insert(Deposito deposito)
        {
            _connection.Execute(@"insert into mgdbm.dbm_deposito( dep_in_codigo
                                                                , dep_st_nome
                                                                , dep_dt_cadastro
                                                                , dep_re_area
                                                                , dep_ch_status
                                                                , org_tab_in_codigo
                                                                , org_pad_in_codigo
                                                                , org_in_codigo
                                                                , org_tau_st_codigo
                                                                , est_in_codigo
                                                                , dep_ch_tipoacesso)
                                                            values
                                                                    (:Id
                                                                , :Nome
                                                                , :DataCadastro
                                                                , :Area
                                                                , :Status
                                                                , :EmpTabela
                                                                , :EmpPadrao
                                                                , :EmpCodigo
                                                                , :EmpTipo
                                                                , :EmpId
                                                                , :Acesso)"
                                    , new
                                    {
                                        Id = deposito.Id,
                                        Nome = deposito.Nome,
                                        DataCadastro = deposito.Data,
                                        Area = deposito.Area,
                                        Status = deposito.Status,
                                        EmpTabela = deposito.Tab,
                                        EmpPadrao = deposito.Pad,
                                        EmpCodigo = deposito.Org,
                                        EmpTipo = deposito.Tau,
                                        EmpId = deposito.IdEstruturaUnidade,
                                        Acesso = deposito.Acesso
                                    });
        }
    }
}
