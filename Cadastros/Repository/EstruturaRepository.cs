﻿using Cadastros.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cadastros.Models;
using System.Data.Common;
using System.Data;
using Dapper;

namespace Cadastros.Repository
{
    class EstruturaRepository : IEstruturaRepository
    {
        private DbConnection _connect;
        private DbTransaction _transaction;

        public EstruturaRepository(DbConnection connect)
        {
            _connect = connect;
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public Estrutura Get(int id)
        {
            return _connect.Query<Estrutura>(@"SELECT EST_IN_CODIGO as Id
                                                    , EST_ST_CODIGO as Identificador
                                                    , PAI_EST_IN_CODIGO as IdPai
                                                    , EST_CH_TIPOESTRUTURA as TipoEstrutura
                                                    , EST_DT_CADASTRO as DataCadastro
                                                    , EST_ST_NOME as Nome
                                                 FROM mgdbm.dbm_estrutura
                                                WHERE EST_IN_CODIGO = :id"
                                             , new { id }).SingleOrDefault();
        }

        public IEnumerable<Estrutura> Get()
        {
            return _connect.Query<Estrutura>(@"SELECT EST_IN_CODIGO as Id
                                                    , EST_ST_CODIGO as Identificador
                                                    , PAI_EST_IN_CODIGO as IdPai
                                                    , EST_CH_TIPOESTRUTURA as TipoEstrutura
                                                    , EST_DT_CADASTRO as DataCadastro
                                                    , EST_ST_NOME as Nome
                                                 FROM mgdbm.dbm_estrutura");
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public void BeginTransaction()
        {
            _transaction = _connect.BeginTransaction();
        }

        public Estrutura GetUnidadeFromUnidade(int unidade)
        {
            return _connect.Query<Estrutura>( @"SELECT UND.EST_IN_CODIGO as Id
                                                     , UND.EST_ST_CODIGO as Identificador
                                                     , UND.PAI_EST_IN_CODIGO as IdPai
                                                     , UND.EST_CH_TIPOESTRUTURA as TipoEstrutura
                                                     , UND.EST_DT_CADASTRO as DataCadastro
                                                     , UND.EST_ST_NOME as Nome
                                                  FROM MGDBM.DBM_ESTRUTURA UND
                                                 WHERE UND.EST_CH_TIPOESTRUTURA = 'U'
                                                   AND UND.EST_IN_CODIGO = :unidade"
                                              ,new { unidade }).SingleOrDefault();
        }

        public Estrutura GetBlocoFromUnidade(int unidade)
        {
            return _connect.Query<Estrutura>(@"SELECT  BLO.EST_IN_CODIGO AS ID
                                                     , BLO.EST_ST_CODIGO AS IDENTIFICADOR
                                                     , BLO.PAI_EST_IN_CODIGO AS IDPAI
                                                     , BLO.EST_CH_TIPOESTRUTURA AS TIPOESTRUTURA
                                                     , BLO.EST_DT_CADASTRO AS DATACADASTRO
                                                     , BLO.EST_ST_NOME AS NOME
                                                  FROM MGDBM.DBM_ESTRUTURA UND
                                                INNER JOIN MGDBM.DBM_ESTRUTURA BLO ON BLO.EST_IN_CODIGO = UND.PAI_EST_IN_CODIGO
                                                 WHERE UND.EST_CH_TIPOESTRUTURA = 'U'
                                                   AND UND.EST_IN_CODIGO = :unidade
                                                   AND BLO.EST_CH_TIPOESTRUTURA = 'B'"
                                              , new { unidade }).SingleOrDefault();
        }

        public Estrutura GetEmpreendimentoFromUnidade(int unidade)
        {
            return _connect.Query<Estrutura>(@"SELECT  EMP.EST_IN_CODIGO AS ID
                                                     , EMP.EST_ST_CODIGO AS IDENTIFICADOR
                                                     , EMP.PAI_EST_IN_CODIGO AS IDPAI
                                                     , EMP.EST_CH_TIPOESTRUTURA AS TIPOESTRUTURA
                                                     , EMP.EST_DT_CADASTRO AS DATACADASTRO
                                                     , EMP.EST_ST_NOME AS NOME
                                                  FROM MGDBM.DBM_ESTRUTURA UND
                                                INNER JOIN MGDBM.DBM_ESTRUTURA BLO ON BLO.EST_IN_CODIGO = UND.PAI_EST_IN_CODIGO
                                                INNER JOIN MGDBM.DBM_ESTRUTURA ETP ON ETP.EST_IN_CODIGO = BLO.PAI_EST_IN_CODIGO
                                                INNER JOIN MGDBM.DBM_ESTRUTURA EMP ON EMP.EST_IN_CODIGO = ETP.PAI_EST_IN_CODIGO
                                                 WHERE UND.EST_CH_TIPOESTRUTURA = 'U'
                                                   AND UND.EST_IN_CODIGO = :unidade
                                                   AND BLO.EST_CH_TIPOESTRUTURA = 'B'   
                                                   AND ETP.EST_CH_TIPOESTRUTURA = 'T'
                                                   AND EMP.EST_CH_TIPOESTRUTURA = 'E'"
                                              , new { unidade }).SingleOrDefault();
        }

        public Estrutura GetEtapaFromUnidade(int unidade)
        {
            return _connect.Query<Estrutura>(@"SELECT  ETP.EST_IN_CODIGO AS ID
                                                     , ETP.EST_ST_CODIGO AS IDENTIFICADOR
                                                     , ETP.PAI_EST_IN_CODIGO AS IDPAI
                                                     , ETP.EST_CH_TIPOESTRUTURA AS TIPOESTRUTURA
                                                     , ETP.EST_DT_CADASTRO AS DATACADASTRO
                                                     , ETP.EST_ST_NOME AS NOME
                                                  FROM MGDBM.DBM_ESTRUTURA UND
                                                INNER JOIN MGDBM.DBM_ESTRUTURA BLO ON BLO.EST_IN_CODIGO = UND.PAI_EST_IN_CODIGO
                                                INNER JOIN MGDBM.DBM_ESTRUTURA ETP ON ETP.EST_IN_CODIGO = BLO.PAI_EST_IN_CODIGO
                                                 WHERE UND.EST_CH_TIPOESTRUTURA = 'U'
                                                   AND UND.EST_IN_CODIGO = :unidade
                                                   AND BLO.EST_CH_TIPOESTRUTURA = 'B'   
                                                   AND ETP.EST_CH_TIPOESTRUTURA = 'T'"
                                              , new { unidade }).SingleOrDefault();
        }
    }
}
