﻿using Cadastros.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Interfaces
{
    public interface IEstruturaRepository
    {
        Estrutura Get(int id);
        IEnumerable<Estrutura> Get();
        Estrutura GetUnidadeFromUnidade(int unidade);
        Estrutura GetBlocoFromUnidade(int unidade);
        Estrutura GetEmpreendimentoFromUnidade(int unidade);
        Estrutura GetEtapaFromUnidade(int unidade);
        void BeginTransaction();
        void Commit();
        void Rollback();
    }
}
