﻿using Cadastros.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Interfaces
{
    public interface IDepositoRepository
    {
        IEnumerable<Deposito> Get();
        Deposito Get(int id);
        void Insert(Deposito deposito);
        void Delete(int id);
        void Edit(Deposito Deposito);

    }
}
