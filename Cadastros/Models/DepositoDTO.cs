﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Models
{
    public class DepositoDTO : Deposito
    {
        public DepositoDTO(Deposito deposito)
        {
            this.Id = deposito.Id;
            this.Nome = deposito.Nome;
            this.Data = deposito.Data;
            this.Area = deposito.Area;
            this.Status = deposito.Status;
            this.Tab = deposito.Tab;
            this.Pad = deposito.Pad;
            this.Org = deposito.Org;
            this.Tau = deposito.Tau;
            this.IdEstruturaUnidade = deposito.IdEstruturaUnidade;
            this.Acesso = deposito.Acesso;
        }

        public DepositoDTO()
        {

        }
        public string NomeEmpreendimento { get; set; }
        public string NomeBloco { get; set; }
        public string NomeUnidade { get; set; }
    }
}
