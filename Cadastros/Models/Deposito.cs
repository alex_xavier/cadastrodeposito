﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Models
{
    public class Deposito
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime Data { get; set; }
        public int Area { get; set; }
        public string Status { get; set; }
        public int Tab { get; set; }
        public int Pad { get; set; }
        public int Org { get; set; }
        public string Tau { get; set; }
        public int IdEstruturaUnidade { get; set; }
        public string Acesso { get; set; }

    }
}
