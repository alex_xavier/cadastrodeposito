﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Models
{
    public class EstruturaSoftDTO
    {
        public decimal Id { get; set; }
        public string Nome { get; set; }
    }
}
