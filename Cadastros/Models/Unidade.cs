﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Models
{
    public class Unidade
    {
        public enum TipoStatus
        {
            Disponivel = 'D',
            Indisponivel = 'I',
            Reservada = 'R',
            Vendida = 'V',
            Permutada = 'P',
            Alugada = 'A',
        }

        public decimal Tab { set; get; }
        public decimal Org { set; get; }
        public string Tau { set; get; }
        public decimal IdUnidade { set; get; }
        public decimal IdEmpreendimento { set; get; }
        public decimal IdEtapa { set; get; }
        public decimal IdTipologia { set; get; }
        public TipoStatus Status { set; get; }
        public DateTime DataStatus { set; get; }
        public decimal Valor { set; get; }
        public decimal Andar { set; get; }
        public decimal AreaPrivativa { set; get; }
        public decimal AreaComum { set; get; }
        public decimal AreaTotal { set; get; }
        public decimal AreaFracao { set; get; }
        public decimal Quota { set; get; }
        public decimal Peso { set; get; }
        public decimal Dependencias { set; get; }
        public decimal AreaGaragem { set; get; }
        public decimal AreaOutros { set; get; }
        public decimal AreaTerraco { set; get; }
        public decimal FracaoIdeal { set; get; }
        public decimal IdQuadroAreas { set; get; }
        public decimal IdBloco { set; get; }
        public string Matricula { set; get; }
        public DateTime DataMatricula { set; get; }
        public string LocalMatricula { set; get; }
        public decimal IdentificacaoDeposito { set; get; }
        public bool ConsideraInvestidor { set; get; }
        public string RegistroMatricula { set; get; }
        public string Alvara { set; get; }
        public DateTime DataAlvara { set; get; }
        public string ProcessoAlvara { set; get; }
        public string PrefeituraAlvara { set; get; }
        public bool ConsideraInvestidorFinanceiro { set; get; }
        public decimal IdProposta { set; get; }
        public bool PossuiCustoREF { set; get; }
        public string Agrupador { set; get; }
        public decimal ValorM2 { set; get; }
        public bool UsaAreaPrivatvaPeso { set; get; }
        public bool UsaAreaComumPeso { set; get; }
        public bool UsaAreaGaragemPeso { set; get; }
        public bool UsaAreaTerracoPeso { set; get; }
        public bool UsaAreaOutrosPeso { set; get; }
        public decimal GaragensVinculadas { set; get; }
        public decimal FracaoTerreno { set; get; }
        public decimal AreaDeposito { set; get; }
        public string LocalDeposito { set; get; }
        public decimal FracaoSocietario { set; get; }
        public decimal FracaoTerrenoSocietario { set; get; }
        public string InscricaoMunicipal { set; get; }
        public DateTime DataEntregaEfetica { set; get; }
        public string InformacaoComplementarSPEED { set; get; }
        public decimal ValorIPTU { set; get; }
        public decimal ValorVendal { set; get; }
        public bool PossuiTermoHipoteca { set; get; }
        public bool PossuiTermoAnuencia { set; get; }
        public decimal ValorReal { set; get; }
        public string CodigoDAC10 { set; get; }
        public decimal ValorAvaliacao { set; get; }
        public bool PossuiEnderecoProprio { set; get; }
        public decimal IdClassificacao { set; get; }
        public decimal IdRedApropriacaoImobiliaria { set; get; }
        public string ProjetoApropriacaoImobiliaria { set; get; }
        public decimal PadApropriacaoImobiliaria { set; get; }
        public decimal TabApropriacaoImobiliaria { set; get; }
        public string DescricaoProjeto { set; get; }
        public decimal IdStatusEspelhoVendas { set; get; }
        public bool NaoValidarStatusContrato { set; get; }
        public string IndicacaoFiscal { set; get; }
        public decimal TabIPTU { set; get; }
        public decimal PadIPTU { set; get; }
        public string IdentificadorIPTU { set; get; }
        public decimal CentroCustoReduzidoIPTU { set; get; }
        public bool UtilizaCobrancaCompartilhada { set; get; }
        public decimal IdContratoBancario { set; get; }
        public decimal IdContratoCobranca { set; get; }
        public decimal TabContaFinanceira { set; get; }
        public decimal PadContaFinanceira { set; get; }
        public decimal IdContaFinanceira { set; get; }
        public decimal TauContaFinanceira { set; get; }
    }
}
