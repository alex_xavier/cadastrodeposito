﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Models
{
    public class Estrutura
    {
        public decimal Id { get; set; }
        public string Identificador { get; set; }
        public decimal IdPai { get; set; }
        public string TipoEstrutura { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Nome { get; set; }
    }
}
