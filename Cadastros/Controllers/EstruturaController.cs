using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Cadastros.Service;
using Cadastros.Models;

namespace Cadastros.Controllers
{
    //[Produces("application/json")]
    [Route("api/cadastro/[controller]")]
    public class EstruturaController : Controller
    {
        private EstruturaService _service;

        public EstruturaController(EstruturaService service)
        {
            _service = service;
        }

        // GET: api/Estrutura
        [HttpGet]
        public IEnumerable<Estrutura> Get()
        {
            return _service.getAll();
        }

        // GET: api/Estrutura/5
        [HttpGet("{id}", Name = "Get")]
        public Estrutura Get(int id)
        {
            return _service.get(id);
        }

        // GET: api/Estrutura/5
        [HttpGet("UnidadeFromUnidade/{id}")]
        public Estrutura GetUnidadeFromUnidade(int id)
        {
            return _service.getUnidadeFromUnidade(id);
        }

        // GET: api/Estrutura/5
        [HttpGet("BlocoFromUnidade/{id}")]
        public Estrutura GetBlocoFromUnidade(int id)
        {
            return _service.getBlocoFromUnidade(id);
        }

        // GET: api/Estrutura/5
        [HttpGet("EmpreendimentoFromUnidade/{id}")]
        public Estrutura GetEmpreendimentoFromUnidade(int id)
        {
            return _service.getEmpreendimentoFromUnidade(id);
        }

        // POST: api/Estrutura
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Estrutura/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
