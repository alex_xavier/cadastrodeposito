using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Cadastros.Service;
using Cadastros.Models;

namespace Cadastros.Controllers
{
    [Produces("application/json")]
    [Route("api/cadastro/[controller]")]    
    public class DepositoController : Controller
    {
        private DepositoService _service;

        public DepositoController(DepositoService service)
        {
            _service = service;
        }

        // GET: api/cadastro/Deposito
        [HttpGet(Name = "GetAllDeposito")]
        public IEnumerable<Deposito> Get()
        {
            return _service.GetAll();
        }
        // GET: api/cadastro/Deposito
        [HttpGet("detail", Name = "GetAllDepositoDetail")]
        public IEnumerable<DepositoDTO> GetAllDetail()
        {
            return _service.GetAllDetail();
        }

        // GET: api/Deposito/detail/5
        [HttpGet("detail/{id}", Name = "GetDeposito")]
        public DepositoDTO Get(int id)
        {
            return _service.GetDetail(id);
        }

        // POST: api/Deposito/insert/
        [HttpPost("inserts")]
        public IEnumerable<string> Post([FromBody]IEnumerable<Deposito> value)
        {
            if (value is null)
                return new List<string> { "Passagem de parāmetros incorreto" };
            else
                return _service.Insert(value);
        }

        // POST: api/Deposito/insert/
        [HttpPost("insert")]
        public IEnumerable<string> Post([FromBody]Deposito value)
        {
            if (value is null)
                return new List<string> { "Passagem de parāmetros incorreto" };
            else
                return _service.Insert(new List<Deposito> { value });
        }

        // POST: api/Deposito/edit/
        [HttpPost("edit")]
        public IEnumerable<string> Edit([FromBody]Deposito value)
        {
            if (value is null)
                return new List<string> { "Passagem de parāmetros incorreto" };
            else
                return _service.Edit(new List<Deposito> { value });
        }

        // POST: api/Deposito/edits/
        [HttpPost("edits")]
        public IEnumerable<string> Edit([FromBody]IEnumerable<Deposito> value)
        {
            if (value is null)
                return new List<string> { "Passagem de parāmetros incorreto" };
            else
                return _service.Edit(value);
        }

        // PUT: api/Deposito/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
