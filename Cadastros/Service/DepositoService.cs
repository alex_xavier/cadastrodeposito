﻿using Cadastros.Interfaces;
using Cadastros.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Service
{
    public class DepositoService
    {
        private IDepositoRepository _repository;
        private EstruturaService _estruturaService;

        public DepositoService(IDepositoRepository repository, EstruturaService estruturaService)
        {
            _repository = repository;
            _estruturaService = estruturaService;
        }

        public IEnumerable<Deposito> GetAll()
        {
            return _repository.Get();
        }

        private DepositoDTO FillDepositoDTO(Deposito deposito)
        {
            DepositoDTO depositoDetail;
            if (deposito is null)
                return null;
            //Preciso verificar se a busca não retorna nulo!
            Estrutura busca = _estruturaService.getUnidadeFromUnidade(deposito.IdEstruturaUnidade);
            if (busca is null)
                depositoDetail = new DepositoDTO(deposito);
            else
            {
                depositoDetail = new DepositoDTO(deposito)
                {
                    NomeBloco = _estruturaService.getBlocoFromUnidade(deposito.IdEstruturaUnidade).Nome,
                    NomeEmpreendimento = _estruturaService.getEmpreendimentoFromUnidade(deposito.IdEstruturaUnidade).Nome,
                    NomeUnidade = busca.Nome,
                };
            }

            return depositoDetail;
        }

        public IEnumerable<DepositoDTO> GetAllDetail()
        {
            //throw new NotImplementedException();
            foreach (Deposito deposito in GetAll())
            {
                //Aqui deveria chamar outro API Service, porém como é o mesmo processo
                //"Cadastro" optei por utilizar o serviço já implementado.
                yield return FillDepositoDTO(deposito);
            }
        }

        public DepositoDTO GetDetail(int id)
        {
            return FillDepositoDTO(_repository.Get(id));
        }

        public IEnumerable<string> Insert(IEnumerable<Deposito> depositos)
        {
            List<string> returns = new List<string>();
            foreach (Deposito deposito in depositos)
            {
                try
                {
                    _repository.Insert(deposito);
                    returns.Add(string.Format("Deposito {0} inserido com sucesso.", deposito.Id));
                } catch (Exception e)
                {
                    returns.Add(string.Format("ERRO: Ao inserir deposito: {0}. Detalhes: {1}.", deposito.Id, e.Message));
                }
            }
            return returns;
        }

        public IEnumerable<string> Edit(IEnumerable<Deposito> depositos)
        {
            List<string> returns = new List<string>();
            foreach (Deposito deposito in depositos)
            {
                try
                {
                    if (_repository.Get(deposito.Id) is null)
                    {
                        returns.Add(string.Format("Deposito {0} não existe!.", deposito.Id));
                        continue;
                    }
                    _repository.Edit(deposito);
                    returns.Add(string.Format("Deposito {0} alterado com sucesso.", deposito.Id));
                }
                catch (Exception e)
                {
                    returns.Add(string.Format("ERRO: Ao alterar deposito: {0}. Detalhes: {1}.", deposito.Id, e.Message));
                }
            }
            return returns;
        }

    }
}
