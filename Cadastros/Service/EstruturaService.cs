﻿using Cadastros.Interfaces;
using Cadastros.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cadastros.Service
{
    public class EstruturaService
    {
        private IEstruturaRepository _repository;

        public EstruturaService(IEstruturaRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Estrutura> getAll()
        {
            return _repository.Get();
        }

        public Estrutura get(int id)
        {
            return _repository.Get(id);
        }

        public Estrutura getUnidadeFromUnidade(int unidade)
        {
            return _repository.GetUnidadeFromUnidade(unidade);
        }

        public Estrutura getBlocoFromUnidade(int unidade)
        {
            return _repository.GetBlocoFromUnidade(unidade);
        }

        public Estrutura getEmpreendimentoFromUnidade(int unidade)
        {
            return _repository.GetEmpreendimentoFromUnidade(unidade);
        }
    }
}
